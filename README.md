A repository containing recipes for Conda packages
==================================================

[Conda](http://conda.pydata.org/) is package manager for arbitrary software. It is increasingly popular, especially in the scientific community.
With conda, is is extremely easy to create, distribute and install packages in any environment and operating system.
This repository is dedicated to hosting recipes for conda packages that are still missing in the web and needed for our own software.
The packages shall be uploaded to the [liulab conda channel](https://binstar.org/organization/liulab/dashboard).
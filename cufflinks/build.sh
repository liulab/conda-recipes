#!/bin/bash

mkdir $PREFIX/bin

cp cufflinks $PREFIX/bin
cp cuffcompare $PREFIX/bin
cp cuffdiff $PREFIX/bin
cp cuffmerge $PREFIX/bin
cp gffread $PREFIX/bin
cp gtf_to_sam $PREFIX/bin
cp cuffnorm $PREFIX/bin
cp cuffquant $PREFIX/bin

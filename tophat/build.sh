
mkdir -p $PREFIX
mkdir -p $PREFIX/lib/tophat
mkdir -p $PREFIX/bin

cp -r * $PREFIX/lib/tophat

cd $PREFIX/bin
ln -s ../lib/tophat/tophat tophat
ln -s ../lib/tophat/tophat-fusion-post tophat-fusion-post
